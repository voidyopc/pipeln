# pipeln

simple todo lists on the command line

# Description

Create simple TODO lists, manipulate them by prepending and queueing
items, marking them done or skipped.

# Usage 

	% pipeln [<action> [<params>]]

where `action` and `parameters` are as detailed in the `case..esac` at
the bottom of the script. (s. full list of supported actions further
down in this usage message.)

Use `pipeln.foo` as symlink to `pipeln` in order to work with project
`foo` on file `TODO.foo`. The link will be set up if you use `pipeln
create` assumming that `/home/sdragiev/bin/` is on the PATH. The list files live
in `~/.pipeln/`.

Check out pipeln with no args to get a table overview of all pipelns.

Ultimately, look in the source code for more details.

## example 
	
	% pipeln create bike		# create a `project`, i.e. a list
	% pipeln.bike q "buy bike"	# add todos
	% pipeln.bike q "ride to the lake"
	% pipeln.bike did		# mark as done: bought bike 
	% pipeln.bike pu "check tyres"  # first thing first
	% pipeln.bike did		# checked tyres. have flat one
	% pipeln.bike pu repair		# before riding to the lake
	% pipeln.bike did		# repaired
	% pipeln.bike q "swim" 		# after riding to the lake
	% pipeln.bike q "ride back"	# after swimming
	% pipeln.bike q "have a meal"	# when back
	% pipeln.bike did		# had a swim
	% pipeln.bike did		# went back
	% pipeln.bike did		# had a meal
	% pipeln.bike done		# review your achievement

## actions 

### project actions
	todo)	# show todos
	n|next)	# show first todo
	done)	# show done
	did|pop) # mark first done
	skip) # skip first (like did but prepend dot)
	a*)	# show all
	e|edit)	# edit raw file
	pu|push)	# add todo on top
	q|queue) 	# add todo at bottom
	del|delete)	# delete line
	mv|move) # move line from to
	w|work) # start working on this project (mark "in progress")
	s|sw|stopwork|l|le|lea|leave) # stop working on this, move to other prjct
	bg|bgr|background) # put project in background
	fg|foreground) # put project in foreground
	h|help|-h|--help)	# show help	
	*) die unknown action

### global actions
		h|help|-h|--help)
		raw) # a non-pretty-printed summary of all pipelns
		'') # (no params) a pretty-printed summary of all pipelns
		create) # create new TODO file
		ren|rename)	# rename project
		rem|remove)	# remove project
		off)	# switch off a project (don't show)
		ls|list|projects)
		*)

# File format

The todos are kept in plain text files with simple format:

- A todo item per line.
- When done, the top item is moved below the `DONE` line.
- If skipped, the top item is prepended a dot and moved below the `DONE`
  line.

# Bloat

Honestly, all these `work`, `stopwork`, `leave`, `background` actions
sounded like fun but now I consider them  bloat; expect them to be
removed.

The `mv` and `del` actions, save you a few key strokes if you are
sure about the synthax, but you'll probably find yourself `edit`ing the
raw file with your favourite editor rather than that.
