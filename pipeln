#!/bin/dash

PIPELNHOME=${PIPELNHOME:-"$HOME/.pipeln"}
FN="$PIPELNHOME/TODO"

we=`basename $0`

die () {

	echo "$we: $@"
	exit 0

}

usage () {

	cat <<.
Description:

Create simple TODO lists, manipulate them by prepending and
queueing items, marking them done. Doesn't support dependencies between
items.

Usage:
	% $we [<action> [<params>]]

where <action> and <parameters> are as detailed in the 'case..esac' at
the bottom of the script. (s. full list of supported actions further
down in this usage message.)

Use 'pipeln.foo' as symlink to 'pipeln' in order to work with project
'foo' on file 'TODO.foo'. The link will be set up if you use 'pipeln
create' assumming that '$HOME/bin/' is on the PATH. The list files live
in '~/.pipeln/'.

Check out pipeln with no args to get a table overview of all pipelns.

Ultimately, look in the source code for more details.

(Honestly, all these 'work', 'stopwork', 'leave', 'background' actions
sounded like fun but now I consider them  bloat; expect them to be
removed.)

example:

	% pipeln create bike		# create a 'project', i.e. a list
	% pipeln.bike q "buy bike"	# add todos
	% pipeln.bike q "ride to the lake"
	% pipeln.bike did		# mark as done: bought bike
	% pipeln.bike pu "check tyres"  # first thing first
	% pipeln.bike did		# checked tyres. have flat one
	% pipeln.bike pu repair		# before riding to the lake
	% pipeln.bike did		# repaired
	% pipeln.bike q "swim" 		# after riding to the lake
	% pipeln.bike q "ride back"	# after swimming
	% pipeln.bike q "have a meal"	# when back
	% pipeln.bike did		# had a swim
	% pipeln.bike did		# went back
	% pipeln.bike did		# had a meal
	% pipeln.bike done		# review your achievement

actions:
.
	echo   project actions
	sed -n '/^case/,/^esac/{ /^[^()]*)/ p}' $0
	echo   global actions
	sed -n '/\tcase/,/\tesac/{ /^[^()]*)/ p}' $0
	exit 0
}

spurious () {
	[ -n "$@" ] && msg="\`$we $@\` doesn't expect arguments!"
	die $msg
}

cut_wstr () {
	echo "$1" | sed "s/\(.\{$2\}\).*/\1/"
}

# pretty print summary of all pipelns
todostodos () {

	tabs 12,+42,+10
	echo "
 ┌─────────┬─────────────────────────────────────────┐   
 │   proj  │    TODO next                            │
 ├─────────┼─────────────────────────────────────────┤"
	for i in `ls -t $FN.*`;
	do
		[ "${i##*.}" = "piplnbckp" ] &&  continue
		[ "${i##*.}" = "off" ] &&  continue
		prj_i=`cut_wstr "${i##*.}" 8`
		echo -n " │ $prj_i\t│"
		n=`pipeln.${i##*.} n`
		prj_i=`cut_wstr "$n" 39`
		echo -n " $prj_i\t│"
		echo
	done
	echo "\
 └─────────┴─────────────────────────────────────────┘"

}

raw () {
	[ -n "$1" ] && max=$1 || max=21
	for i in `ls -t $FN.*`;
	do
		[ "${i##*.}" = "piplnbckp" ] &&  continue
		[ "${i##*.}" = "off" ] &&  continue
		echo -n `cut_wstr "${i##*.}" 8` "\t"
		n=`pipeln.${i##*.} n`
		echo `cut_wstr "$n" $max`
	done
}

raw2 () {
	[ -n "$1" ] && max_todos=$1 || max_todos=1
	shift
	[ -n "$1" ] && max_width=$1 || max_width=21
	shift
	[ -n "$1" ] && projs=`for a in $@; do echo -n " -e .$a"; done` || projs=' -e .'
	for i in `ls -t $FN.*| grep $projs`;
	do
		[ "${i##*.}" = "piplnbckp" ] &&  continue
		[ "${i##*.}" = "off" ] &&  continue
		printf "%-20s" "${i##*.}"
		pipeln.${i##*.} n $max_todos\
			| while read n;
				do printf "%-""$max_width.$max_width""s \n" "$n";
			done
	done
}

bounds() {
	f=$1
	t=$2
	# find last todo
	max=`sed -n '/^DONE$/ {=;q}' $file`
	[ -n '$max' ] && max=$(( $max - 1)) || max=`sed '$ ='`
	[ $f -le 0 -o $t -le 0 -o $max -lt $t -o $max -lt $f ] && die "line indexes $t, $f out of bounds [1..$max]"
	[ $t -eq $f ] && die "move to same line doesn't make sense"
}

# read project name from the invoked file name the pattern is:
# pipeln.foo deals with project foo and uses ~/TODO.foo
prj=${we#*.}

# global mode: if invoked without project have following commands:
if [ x"$prj" = x"$we" ]; then
	case "$1" in
		h|help|-h|--help)
			usage
			;;
		raw) # a non-pretty-printed summary of all pipelns
			shift
			raw "$1"
			;;
		'') # (no params) a pretty-printed summary of all pipelns
			todostodos
			;;
		create) # create new TODO file
			[ -z "$2" ] && die "need filename!"
			[ -z "$3" ] || die "too much args to $a: $3!"
			file="$FN.$2"
			[ -e "$file" ] && die "file $file already exists!"
			[ -e "$HOME/bin/$we.$2" ] && die "file $we.$2 already exists!"
			echo DONE > "$file"
			ln -s `which $we` "$HOME/bin/$we.$2"
			;;
		ren|rename)	# rename project
			[ -z "$3" ] && die "need two args!"
			[ -z "$4" ] || die "too much args to $a: $4!"
			file="$FN.$2"
			[ -e "$file" ] || die "project $2 does not exist!"
			err=`"$we" create "$3"`
			[ -z err ] || die "could not create project $3;\n$err"
			newfile="$FN.$3"
			mv "$file" "$newfile"
			rm "$HOME/bin/$we.$2"
			;;
		rem|remove)	# remove project
			[ -z "$2" ] && die "need one arg!"
			[ -z "$3" ] || die "too much args to $a: $3!"
			file="$FN.$2"
			[ -e "$file" ] || die "project $2 does not exist!"
			read -p "deleting project $2. sure? (yes|[no])" a
			[ x"$a" =  x"yes" ] || exit 0
			rm "$file"
			rm "$HOME/bin/$we.$2"
			;;
		off)	# switch off a project (don't show)
			[ -z "$2" ] && die "need one arg!"
			[ -z "$3" ] || die "too much args to $a: $3!"
			file="$FN.$2"
			[ -e "$file" ] || die "project $2 does not exist!"
			read -p "switching off project $2. sure? (yes|[no])" a
			[ x"$a" =  x"yes" ] || exit 0
			mv "$file" "$file".off
			;;
		ls|list|projects)
			shift
			[ x"$1" = x"-a" ] && (shift; all=yes) || all=no
			for i in `ls $FN.* "$@"`;
			do
				[ "${i##*.}" = "piplnbckp" ] &&  continue
				[ "${i##*.}" = "off" -a $all = no ] &&  continue
				echo pipeln."${i##*.}"
			done
			;;
		*)
			die "wrong command with empty projectname:" $@
			;;
	esac
	exit 0
fi

if [ x"$prj" = "x." ]; then
	prj=$1
	shift
fi

file="$FN.$prj"
[ -z "$1" ] && a=todo || a=$1

case $a in
	todo)	# show enumerated todos
		n=1
		[ -n "$2" ] && [ "$2" -gt 1 ] 2>/dev/null && n=$2
		[ -z "$3" ] || spurious "$a"
		sed '/^DONE/,$ d' $file | nl -s' ' -w2 -v0 | sed $n'q'
		;;
	n|next)	# show first todo(s)
		n=1
		[ -n "$2" ] && [ "$2" -gt 1 ] 2>/dev/null && n=$2
		[ -z "$3" ] || spurious "$a"
		sed '/^DONE/,$ d' $file | sed "$n"q
		;;
	done)	# show done
		[ -z "$2" ] || spurious "$a"
		sed '0,/^DONE/ d' $file | cat -n
		;;
	did|pop) # mark i-th done
		[ -z "$3" ] || spurious "$a"
		i=${2:-1}
		n=`sed -n '/^DONE/=' $file`
		[ -z "$n" ] && die "bad file content: /^DONE/ not found"
		[ $n -gt "$i" ] || die "I see no task to be done"
		echo -n "done $i-th: "; sed -n "$i {p;q}" $file
		sed --follow-symlinks -i "${i}h;${i}d;$ G" $file
		echo -n "next: "
		$we n
		;;
	undo|unpop) # undo last
		[ -z "$3" ] || spurious "$a"
		n=`sed -n '/^DONE/=' $file`
		n_all=`sed -n '$ =' $file`
		n_done=$((n_all - n))
		[ -z "$n" ] && die "bad file content: /^DONE/ not found"
		[ $n_done -gt 1 ] || die "I see no task to be done"
		echo '$m1\n1m2\nw' | ed $file
		$we n
		;;
	skip) # skip i-th (like did but prepend dot)
		[ -z "$3" ] || spurious "$a"
		i=${2:-1}
		n=`sed -n '/^DONE/=' $file`
		[ -z "$n" ] && die "bad file content: /^DONE/ not found"
		[ $n -gt "$i" ] || die "I see no task to be skipped"
		echo -n "skipped $i-th: "; sed -n "$i {p;q}" $file
		sed --follow-symlinks -i "${i} s/^/./; ${i}h;${i}d;$ G" $file
		$we n
		;;
	a*)	# show all
		[ -z "$2" ] || spurious "$a"
		cat $file
		;;
	e|edit)	# edit raw file
		[ -z "$2" ] || spurious "$a"
		$EDITOR $file
		;;
	pu|push)	# add todo on top
		[ -z "$2" ] && die "need arg!"
		sed --follow-symlinks -i "1i $2" $file
		;;
	q|queue) 	# add todo at bottom
		[ -z "$2" ] && die "need arg!"
		sed --follow-symlinks -i "/^DONE/i $2" $file
		;;
	del|delete)	# delete line
		[ -z "$2" ] && die "need arg!"
		sed --follow-symlinks -i "$2 d" $file
		$we todo
		;;
	mv|move) # move line from to
		[ -z "$3" ] && die "need arg!"
		f=$2
		t=$3
		bounds "$f" "$t"
		tmp=`sed -n "$f p" $file`
		sed --follow-symlinks -i.1.piplnbckp "$f d" $file # TODO remove backup
		sed --follow-symlinks -i.2.piplnbckp "$t i $tmp\ " $file # TODO remove backup
		;;
	w|work) # start working on this project (mark "in progress")
		[ -z "$2" ] || die "too many args to $a: $2!"
		sed --follow-symlinks -i.piplnbckp "1 s/$/ */" $file # TODO remove backup
		;;
	s|sw|stopwork|l|le|lea|leave) # stop working on this, move to other prjct
		[ -z "$2" ] || die "too many args to $a: $2!"
		sed --follow-symlinks -i.piplnbckp "1 s/ \*$//" $file # TODO remove backup
		;;
	bg|bgr|background) # put project in background
		[ -z "$2" ] || die "too many args to $a: $2!"
		sed --follow-symlinks -i.piplnbckp "1 s/$/ \&/" $file # TODO remove backup
		;;
	fg|foreground) # put project in foreground
		[ -z "$2" ] || die "too many args to $a: $2!"
		sed --follow-symlinks -i.piplnbckp "1 s/ \&$//" $file # TODO remove backup
		;;
	off) # turn project off, don't show
		[ -z "$2" ] || die "too many args to $a: $2!"
		pipeln off $prj
		;;
	h|help|-h|--help)	# show help
		usage
		;;
	*) die unknown action
		;;
esac
